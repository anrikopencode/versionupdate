Pod::Spec.new do |s|

  s.name          = 'WPCheckVersion'
  s.version       = '0.0.1'
  s.summary       = 'WPCheckVersion'
  s.description   = <<-DESC
                    WPCheckVersion
                   DESC
  s.homepage      = 'https://gitlab.com/anrikopencode/versionupdate.git'
  s.license       = { :type => 'MIT', :file => 'LICENSE' }
  s.author        = { 'anrik' => '819343052@qq.com' }

  s.platform      = :ios
  s.ios.deployment_target = '8.0'
  s.source        = { :git => 'https://gitlab.com/anrikopencode/versionupdate.git', :tag => s.version }

  s.source_files  = '*.{h,m}'
  s.resource      = '*.{png}','WPCheckVersion.bundle'
  s.resource_bundles = {'WPCheckVersion' => ['*.{storyboard,xib,png}']}

  s.public_header_files = '*.h'

  s.frameworks = 'UIKit'
  s.libraries = 'c'
  s.requires_arc = true

  s.dependency 'Masonry'
  s.dependency 'AFNetworking'
  s.dependency 'Aspects'
end
