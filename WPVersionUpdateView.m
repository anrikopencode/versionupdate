//
//  WPVersionUpdateView.m
//
//  Created by Anrik on 2018/6/12.
//

#import "WPVersionUpdateView.h"
#import <Masonry/Masonry.h>
#import "WPCheckVersionTool.h"

@interface WPVersionUpdateView()
{
    NSURL *_updateURL;
}
@property (weak, nonatomic) IBOutlet UILabel *versionLab;
@property (weak, nonatomic) IBOutlet UITextView *logTV;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *updateBtn;
@property (weak, nonatomic) IBOutlet UIView *showView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@end

@implementation WPVersionUpdateView

+ (instancetype)versionUpdateView {
    NSString * bundlePath = [[NSBundle mainBundle] pathForResource:@"WPCheckVersion" ofType:@"bundle"];
    NSBundle *resourceBundle = [NSBundle bundleWithPath:bundlePath];
    WPVersionUpdateView *v = [[resourceBundle loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    v.frame = [[UIScreen mainScreen] bounds];
    return v;
}

-(void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setForceUpdate:(BOOL)forceUpdate {
    _forceUpdate = forceUpdate;
    if (forceUpdate) {
        self.cancelBtn.hidden = YES;
        [self.updateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(19);
            make.right.mas_equalTo(-19);
            make.height.mas_equalTo(42);
            make.bottom.mas_equalTo(-16);
        }];
    }
}

- (void)setChangelog:(NSString *)changelog {
    changelog = [changelog stringByReplacingOccurrencesOfString:@"#" withString:@""];
    _changelog = changelog;
    self.logTV.text  = changelog;
}

- (void)setInstallUrl:(NSString *)installUrl {
    _installUrl = installUrl;
    NSURL *updateURL = [NSURL URLWithString:installUrl];
    _updateURL = updateURL;
}

- (void)showInView:(UIView *)view {
    if (view) {
        [view addSubview:self];
    }else {
        [[UIApplication sharedApplication].keyWindow addSubview:self];
    }
}

- (void)didMoveToWindow {
    [super didMoveToWindow];
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    self.showView.transform = CGAffineTransformMakeScale(0.01f, 0.01);
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.25];
    self.showView.transform=CGAffineTransformMakeScale(1.0f, 1.0f);
    [UIView commitAnimations];
}

- (IBAction)cancelAction:(id)sender {
    [self dismiView:nil];
    [WPCheckVersionTool sharedInstance].isNextUpdate = YES;
}

- (IBAction)updateAction:(id)sender {
    [self dismiView:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([[UIApplication sharedApplication] openURL:_updateURL]) {
            [[UIApplication sharedApplication] performSelector:@selector(suspend)];
            exit(0);
        }
    });
}

- (void)dismiView:(id)sender {
    self.showView.transform = CGAffineTransformMakeScale(1.f, 1.f);
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.25];
    self.showView.transform=CGAffineTransformMakeScale(0.01f, 0.01f);
    [UIView commitAnimations];
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)dealloc {
    NSLog(@"%s",__func__);
}

@end
