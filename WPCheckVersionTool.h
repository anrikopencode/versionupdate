//
//  WPCheckVersionTool.h
//
//  Created by Anrik on 2016/7/13.
//

#import <Foundation/Foundation.h>

//#define kHarpy  // iOS版本更新工具开关

#ifdef kHarpy
#import "Harpy.h"
#endif

#ifdef kHarpy

#endif

#define kAPPLEID @"xxxxx"// AppStore上Apple ID

@interface WPCheckVersionTool : NSObject

/** 是否点击了下次更新 */
@property (nonatomic, assign)BOOL isNextUpdate;

+ (instancetype)sharedInstance;

/** 检测版本升级(注意:在上架到App Store 之前去掉升级检测) */
- (void)checkVersionFromFir;
- (void)checkVersionFromAppStore;

#ifdef kHarpy
-(void)checkVersionFromHarpy;
- (void)checkVersionDaily_Harpy;
- (void)checkVersionWeekly_Harpy;
#endif
@end
