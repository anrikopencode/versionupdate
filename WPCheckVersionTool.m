//
//  WPCheckVersionTool.m
//
//  Created by Anrik on 2016/7/13.
//

#import "WPCheckVersionTool.h"
#import "WPVersionUpdateView.h"
#import <Aspects.h>

#define kFirAlertViewtag 20161208  // firTag值
#define kAppStoreAlertViewtag 20161209  // AppStoreTag值

//@import FirebaseAnalytics;

@interface WPCheckVersionTool ()<UIAlertViewDelegate
#ifdef kHarpy
, HarpyDelegate
#endif
>
{
    NSURL *_updateURL;
    NSString *_log;
}
@property (nonatomic, copy) NSString *appID;// app的ID
@property (nonatomic, copy) NSString *updateButtonText;
@property (nonatomic, copy) NSString *nextTimeButtonText;
@property (nonatomic, copy) NSString *skipButtonText;
@property (nonatomic, assign) BOOL isShow;
@end

@implementation WPCheckVersionTool

+ (NSString *)apiToken {
    return @"49f94af15a41ec9d2812ea1779ab7e29";
}

+ (void)load {
    [self hookUIApplication];
}

+ (void)hookUIApplication {
    Class AppDelegate = NSClassFromString(@"AppDelegate");
    if (AppDelegate) {
        [AppDelegate aspect_hookSelector:@selector(applicationDidBecomeActive:) withOptions:AspectPositionAfter usingBlock:^(){
            if (![WPCheckVersionTool sharedInstance].isNextUpdate) {
                [[WPCheckVersionTool sharedInstance] checkVersionFromFir];
                [WPCheckVersionTool sharedInstance].isNextUpdate = NO;// 防止打开app的时候请求两次
            }
        } error:nil];
    }
}

+ (instancetype)sharedInstance{
    static WPCheckVersionTool *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

- (NSString *)updateButtonText {
    if (!_updateButtonText) {
        _updateButtonText = @"更新";
    }
    return _updateButtonText;
}

- (NSString *)nextTimeButtonText {
    if (!_nextTimeButtonText) {
        _nextTimeButtonText = @"下一次";
    }
    return _nextTimeButtonText;
}

- (NSString *)skipButtonText {
    if (!_skipButtonText) {
        _skipButtonText = @"跳过此版本";
    }
    return _skipButtonText;
}

#pragma mark- 从Fir获取最新的版本数据
/** 从Fir获取最新的版本数据 */
- (void)checkVersionFromFir{
    /*
     id	String	与 bundle_id 二选一必填	应用ID
     bundle_id	String	与 id 二选一必填	Bundle ID(iOS) / Package name(Android)
     
     id    @"5785ac6bf2fc426361000048";
     bundle_id  [[NSBundle mainBundle] infoDictionary][@"CFBundleIdentifier"]
     */
    NSString *appID = [[NSBundle mainBundle] infoDictionary][@"CFBundleIdentifier"];
    NSString *apiToken = [WPCheckVersionTool apiToken];
    NSString *url = [NSString stringWithFormat:@"https://api.fir.im/apps/latest/%@?apiToken=%@",appID,apiToken];
    NSURL *url_ = [NSURL URLWithString:url];
    NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:url_];
    [mutableRequest setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionTask *task = [session dataTaskWithRequest:mutableRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error == nil) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
//            NSLog(@"dict = %@", dict);
            NSDictionary *responseObject = dict;
            NSString *localShortVersion =  [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"];
            NSString *serverShortVersion =  [responseObject valueForKey:@"versionShort"];// 服务器的
            if ([localShortVersion compare:serverShortVersion options:NSNumericSearch] == NSOrderedDescending){
                return; // 本地版本高于服务器版本
            }
            else if([localShortVersion compare:serverShortVersion options:NSNumericSearch] == NSOrderedSame){
                // 本地版本和服务器版本一致,进行build版本对比
                if ([[responseObject objectForKey:@"build"] doubleValue] <= [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] doubleValue]) {
                    return;
                }
            }else if ([localShortVersion compare:serverShortVersion options:NSNumericSearch] == NSOrderedAscending){
                //本地版本低于服务器版本,直接提示升级
            }
            NSString *url = nil;
            
#ifndef kAppStore
            // 20161219针对被拒的问题处理
            
            // 能到达这里说明有版本要升级
            url = [responseObject objectForKey:@"installUrl"];
            if (![url hasPrefix:@"itms-services://"]) {
                url = [NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@",[self encodeUrl:url]];
            }
#else
            
            url = @"";
#endif
            
            NSURL *updateURL = [NSURL URLWithString:url];
            _updateURL = updateURL;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                long long fsize = [[[responseObject valueForKey:@"binary"] objectForKey:@"fsize"] longLongValue];
                NSString *filesize = [NSByteCountFormatter stringFromByteCount:fsize countStyle:NSByteCountFormatterCountStyleFile];
                NSString *text = [NSString stringWithFormat:@"version:%@(build:%@) \n大小:%@\n\n更新日志:\n%@",[responseObject valueForKey:@"versionShort"],responseObject[@"build"],filesize,[[[[responseObject valueForKey:@"changelog"] stringByReplacingOccurrencesOfString:@"#" withString:@""] stringByReplacingOccurrencesOfString:@"@" withString:@""] stringByReplacingOccurrencesOfString:@"~" withString:@""]];
                /*
                 企业版这样提示:
                 NSString *text = [NSString stringWithFormat:@"新版本:%@  build:%@  大小:%@\n%@\n升级后需要打开iPhone的设置->通用->描述文件 对企业级应用进行信任后才能正常使用",[responseObject objectForKey:@"versionShort"],responseObject[@"build"],filesize,[responseObject objectForKey:@"changelog"]];
                 */
                _log =  [responseObject objectForKey:@"changelog"];
                
                if (!self.isShow) {
                    WPVersionUpdateView *versionUpdateView = [WPVersionUpdateView versionUpdateView];
                    versionUpdateView.changelog = text;
                    versionUpdateView.installUrl = url;
                    if ([_log containsString:@"#"]) {
                        versionUpdateView.forceUpdate = YES;
                    }else {
                        versionUpdateView.forceUpdate = NO;
                    }
                    [versionUpdateView showInView:nil];
                }
                self.isShow = YES;
                return ;
                
                // log中含有#表示强制更新
                if (!_log) {
                    UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"检测到新版本" message:text delegate:self cancelButtonTitle:self.updateButtonText otherButtonTitles:self.nextTimeButtonText, nil];
                    alertV.tag = kFirAlertViewtag;
                    [alertV show];
                }else {
                    if (_log && [_log rangeOfString:@"#"].location !=NSNotFound) {
                        UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"检测到新版本" message:text delegate:self cancelButtonTitle:nil otherButtonTitles:self.updateButtonText, nil];
                        alertV.tag = kFirAlertViewtag;
                        [alertV show];
                    }else{
                        UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"检测到新版本" message:text delegate:self cancelButtonTitle:self.updateButtonText otherButtonTitles:self.nextTimeButtonText, nil];
                        alertV.tag = kFirAlertViewtag;
                        [alertV show];
                    }
                }
            });
        }
        
    }];
    //6.启动任务
    [task resume];
}

#pragma mark- 从App Store获取最新的数据
/** 从App Store获取最新的数据 */
-(void)checkVersionFromAppStore {
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    //CFShow((__bridge CFTypeRef)(infoDic));
    NSString *currentVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    
    // 查看应用的详细信息
    /*
     两种形式来访问
     通过包名:https://itunes.apple.com/lookup?bundleId=com.xxx.yyy
     通过appleId:http://itunes.apple.com/lookup?id=xxxxxx
     */
    
    //    NSString *URL = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=%@",kAPPLEID]; //通过appleId
    NSString *URL = [NSString stringWithFormat:@"https://itunes.apple.com/lookup?bundleId=%@",[NSBundle mainBundle].bundleIdentifier];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:URL]];
    [request setHTTPMethod:@"POST"];
    __block NSHTTPURLResponse *urlResponse = nil;
    __block NSError *error = nil;
    __block NSData *recervedData;
    
    dispatch_queue_t q = dispatch_queue_create("create", DISPATCH_QUEUE_SERIAL);
    dispatch_async(q, ^{
        recervedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            // 修复重大bug,在没网的情况下导致app崩溃_20161226
            if (!recervedData) {
                return ;
            }
            NSDictionary *recervedDict = [NSJSONSerialization JSONObjectWithData:recervedData options:NSJSONReadingAllowFragments error:nil];
            NSArray *pVersonInfo = [recervedDict objectForKey:@"results"];
            if ([pVersonInfo count]>0) {
                NSDictionary *releaseInfo = [pVersonInfo objectAtIndex:0];
                NSString *appId = [releaseInfo objectForKey:@"trackId"];
                self.appID = appId;
                NSString *lastVersion = [releaseInfo objectForKey:@"version"];
                NSArray* lvarr=[lastVersion componentsSeparatedByString:@"."];
                NSArray* cvarr=[currentVersion componentsSeparatedByString:@"."];
                BOOL update=NO;
                if ([[cvarr objectAtIndex:0] integerValue]<[[lvarr objectAtIndex:0] integerValue]) {
                    update=YES;
                }
                else if([[cvarr objectAtIndex:0] integerValue]==[[lvarr objectAtIndex:0] integerValue])
                {
                    if ([[cvarr objectAtIndex:1] integerValue]<[[lvarr objectAtIndex:1] integerValue]) {
                        update=YES;
                    }
                    else if ([[cvarr objectAtIndex:1] integerValue]==[[lvarr objectAtIndex:1] integerValue])
                    {
                        if (lvarr.count < 3) {
                            if ([[cvarr objectAtIndex:2] integerValue] > 0 ) {
                                update=YES;
                            }
                        }else {
                            if ([[cvarr objectAtIndex:2] integerValue]<[[lvarr objectAtIndex:2] integerValue]) {
                                update=YES;
                            }
                        }
                    }
                }
                
                if (update) {
                    NSString *releaseNotes = [releaseInfo objectForKey:@"releaseNotes"];
                    [self hasUpdateFromAppStoreActionWithReleaseNotes:releaseNotes lastVersion:lastVersion];
                }
            }
        });
    });
}

/** 从AppStore上获取到有更新Actions */
- (void)hasUpdateFromAppStoreActionWithReleaseNotes:(NSString *)releaseNotes lastVersion:(NSString *)lastVersion {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"检测到新版本:%@",lastVersion] message:releaseNotes preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[self nextTimeAlertAction]];
    [alertController addAction:[self updateAlertAction]];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:^{
        
    }];
}

#pragma mark- Harpy通过第三方框架Harpy获取最新的数据
#ifdef kHarpy
/** 通过第三方框架Harpy获取最新的数据 */
-(void)checkVersionFromHarpy {
    // 通过包名:https://itunes.apple.com/lookup?bundleId=[NSBundle mainBundle].bundleIdentifier
    [[Harpy sharedInstance] setPresentingViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
    [[Harpy sharedInstance] setDelegate:self];
    [[Harpy sharedInstance] setAppName:[[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"]];
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeSkip];
    [[Harpy sharedInstance] setForceLanguageLocalization:@"zh-Hans"];
    [[Harpy sharedInstance] checkVersion];
}

- (void)checkVersionDaily_Harpy {
    [[Harpy sharedInstance] checkVersionDaily];
}

- (void)checkVersionWeekly_Harpy {
    [[Harpy sharedInstance] checkVersionWeekly];
}

/*
 // 用户界面展示升级提示对话框
 - (void)harpyDidShowUpdateDialog {
 }
 
 // 用户已经点击升级按钮并且进入到App Sotore
 - (void)harpyUserDidLaunchAppStore {
 }
 
 // 用户已经点击跳过此次版本更新
 - (void)harpyUserDidSkipVersion {
 }
 
 // 用户已经点击取消更行对话框
 - (void)harpyUserDidCancel{
 }
 */
#endif

#pragma mark- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == kFirAlertViewtag) {
        if (buttonIndex == 0) {
            if ([[UIApplication sharedApplication] canOpenURL:_updateURL]) {
//                [MobClick event:@"update"];
                if ([_log rangeOfString:@"@"].location !=NSNotFound) {
                    // 不管服务器响应如何,直接退出
                    // 删除登陆信息

                }
                
                if ([_log rangeOfString:@"~"].location !=NSNotFound) {
                    // 清表,将FMDB表删除

                }
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if ([[UIApplication sharedApplication] openURL:_updateURL]) {
                        [[UIApplication sharedApplication] performSelector:@selector(suspend)];
                        exit(0);
                    }
                });
                
            }else{
                UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"模拟器不支持升级" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                [alertV show];
            }
        }else{
            // 点击了下次更新
            self.isNextUpdate = YES;
        }
    }else if (alertView.tag == kFirAlertViewtag) {
        
    }
}

#pragma mark - UIAlertActions
- (UIAlertAction *)updateAlertAction {
    UIAlertAction *updateAlertAction = [UIAlertAction actionWithTitle:self.updateButtonText style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        self.isNextUpdate = YES;
        NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", self.appID];
        NSURL *iTunesURL = [NSURL URLWithString:iTunesString];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:iTunesURL];
        });
    }];
    return updateAlertAction;
}

- (UIAlertAction *)nextTimeAlertAction {
    UIAlertAction *nextTimeAlertAction = [UIAlertAction actionWithTitle:self.nextTimeButtonText style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // 点击了下次更新
        self.isNextUpdate = YES;
    }];
    return nextTimeAlertAction;
}

- (UIAlertAction *)skipAlertAction {
    UIAlertAction *skipAlertAction = [UIAlertAction actionWithTitle:self.skipButtonText style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }];
    return skipAlertAction;
}

- (NSString *)encodeUrl:(NSString *)url{
    return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)url, NULL, CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
}

@end
