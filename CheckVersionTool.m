//
//  WPCheckVersionTool.m
//
//  Created by Anrik on 2016/7/13.
//

#import "CheckVersionTool.h"
#import "WPVersionUpdateView.h"

static NSString *kAppleID = @"888";// AppStore上Apple ID

@interface CheckVersionTool ()
{
    NSURL *_updateURL;
    NSString *_log;
}
@property (nonatomic, assign) BOOL isShow;
@end

@implementation CheckVersionTool

+ (instancetype)sharedInstance{
    static CheckVersionTool *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

/** 从服务器获取最新的版本数据 */
- (void)checkVersionFromServerWithInfo:(NSDictionary *)info {
    /*
     */
    if (![[info valueForKey:@"channel"] isEqualToString:@"App Store"]) {
//        NSLog(@"不是App Store渠道");
        return;
    }
    
    NSString *localShortVersion =  [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"];
    NSString *serverShortVersion =  [info valueForKey:@"version"];// 服务器的
    serverShortVersion = [serverShortVersion stringByReplacingOccurrencesOfString:@"v" withString:@""];
    NSLog(@"local:%@",localShortVersion);
    NSLog(@"server:%@",serverShortVersion);
    if ([localShortVersion compare:serverShortVersion options:NSNumericSearch] == NSOrderedDescending) {
        return; // 本地版本高于服务器版本
    }
    else if([localShortVersion compare:serverShortVersion options:NSNumericSearch] == NSOrderedSame){
        // 本地版本和服务器版本一致,进行build版本对比
        if ([[info valueForKey:@"build"] integerValue] <= [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] integerValue]) {
            return;
        }
    }else if ([localShortVersion compare:serverShortVersion options:NSNumericSearch] == NSOrderedAscending){
        //本地版本低于服务器版本,直接提示升级
    }

    BOOL forceflag = [[info valueForKey:@"forceflag"] boolValue];
    if (!self.isShow) {
        WPVersionUpdateView *versionUpdateView = [WPVersionUpdateView versionUpdateView];
        NSString *text = [NSString stringWithFormat:@"%@",[info valueForKey:@"remark"]];
        versionUpdateView.changelog = text;
        NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", kAppleID];
        versionUpdateView.installUrl = iTunesString;
        versionUpdateView.forceUpdate = forceflag;
        [versionUpdateView showInView:nil];
    }
    self.isShow = YES;
}

@end
