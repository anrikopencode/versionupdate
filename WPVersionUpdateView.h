//
//  WPVersionUpdateView.h
//
//  Created by Anrik on 2018/6/12.
//

#import <UIKit/UIKit.h>

@interface WPVersionUpdateView : UIView

@property(nonatomic, assign) BOOL forceUpdate;
@property(nonatomic, copy) NSString *changelog;
@property(nonatomic, copy) NSString *version;
@property(nonatomic, copy) NSString *installUrl;

+ (instancetype)versionUpdateView;
- (void)showInView:(UIView *)view;
@end
