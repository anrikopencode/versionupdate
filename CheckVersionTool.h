//
//  WPCheckVersionTool.h
//
//  Created by Anrik on 2016/7/13.
//

#import <Foundation/Foundation.h>

@interface CheckVersionTool : NSObject

+ (instancetype)sharedInstance;

/** 从服务器获取最新的版本数据进行对比 */
- (void)checkVersionFromServerWithInfo:(NSDictionary *)info;
@end
